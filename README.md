# A Demystification of the Python Language Reference

* explaination of python language reference
* refers to history, computer science theory and mathematics
* new language feature


# Reference Materials

* [The Python Language Reference](https://docs.python.org/3/reference/)
* [The Python Standard Library](https://docs.python.org/3/library/index.html)
* Book: Structure and Interpretation of Computer Programs