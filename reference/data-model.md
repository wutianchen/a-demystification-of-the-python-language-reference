# 3 Data Model

Objects are Python's abstraction for data. All data in a Python program is represented by

* objects or 
* relations between objects

In a sense, and in conformance to Von Neumann's model of a "stored program computer"

## Object

Every object has an identity, a type and a value.

#### identity

An object's identity never changes once it has been created; you may think of it as the object’s address in memory. The ‘is’ operator compares the identity of two objects; the id() function returns an integer representing its identity.


#### type 

An object’s type determines the operations that the object supports (e.g., “does it have a length?”) and also defines the possible values for objects of that type. The type() function returns an object’s type. Like its identity, an object’s type is also unchangeable.


#### value

The value of some objects can change. Objects whose value can change are said to be mutable; objects whose value is unchangeable once they are created are called immutable.

Objects are never explicitly destroyed; however, when they become unreachable they may be garbage-collected.


TODO:
* function id
* function type
* function inspect
